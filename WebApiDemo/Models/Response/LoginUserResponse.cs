﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDemo.Models.Response
{
    public class LoginUserResponse
    {
        /// <summary>
        /// 身份标识Ticket
        /// </summary>
        public string Ticket { get; set; }
    }
}
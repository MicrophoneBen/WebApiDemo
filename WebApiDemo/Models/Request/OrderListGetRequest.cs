﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiDemo.Models.Request
{
    /// <summary>
    /// 订单列表查询参数
    /// </summary>
    public class OrderListGetRequest
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public long OrderId { get; set; }
        /// <summary>
        /// 下单人
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 当前页数
        /// </summary>
        [Required]
        public int Page { get; set; }
        /// <summary>
        /// 每页显示条数
        /// </summary>
        [Required]
        public int Size { get; set; }
    }
}
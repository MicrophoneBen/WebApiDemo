﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDemo.Models.Request;
using WebApiDemo.Models.Response;

namespace WebApiDemo.Controllers
{
    /// <summary>
    /// 会员管理模块
    /// </summary>
    [RoutePrefix("v1")]
    public class UserController : ApiController
    {
        /// <summary>
        /// 会员注册
        /// </summary>
        [Route("user/register")]
        [HttpPost]
        public bool UserRegister(RegisterUserRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Phone)) { throw new CustomException("请输入手机号"); }
            if (string.IsNullOrWhiteSpace(request.Code)) { throw new CustomException("请输入验证码"); }
            if (string.IsNullOrWhiteSpace(request.Password)) { throw new CustomException("请设置密码"); }
            if (request.Type != 1 && request.Type != 2) { throw new CustomException("请选择用户身份类型"); }
            if (string.IsNullOrWhiteSpace(request.CompanyName)) { throw new CustomException("请输入公司全称"); }

            //一系列注册操作

            return true;
        }
        /// <summary>
        /// 会员登录
        /// </summary>
        [Route("user/login")]
        [HttpPost]
        public LoginUserResponse UserLogin(LoginUserRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Phone)) { throw new CustomException("请输入手机号"); }
            if (string.IsNullOrWhiteSpace(request.Code)) { throw new CustomException("请输入验证码"); }
            if (string.IsNullOrWhiteSpace(request.Password)) { throw new CustomException("请输入密码"); }

            //一系列登录操作

            if (true)
            {
                return new LoginUserResponse()
                {
                    Ticket = "KLSJFIKSJFKU5455TF4TRY5Y4RY" //登录成功返回ticket票据
                };
            }
            else
            {
                throw new CustomException("登录失败！请稍后重试！");
            }
        }
    }
}
